-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: Nov 25, 2019 at 06:12 PM
-- Server version: 10.3.14-MariaDB
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lb_janken`
--

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

DROP TABLE IF EXISTS `game`;
CREATE TABLE IF NOT EXISTS `game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `player1_id` int(11) DEFAULT NULL,
  `player2_id` int(11) DEFAULT NULL,
  `game_sequence` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `winner` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `game`
--

INSERT INTO `game` (`id`, `created_at`, `player1_id`, `player2_id`, `game_sequence`, `winner`, `status`) VALUES
(2, '2019-11-25 18:00:57', 1, 2, '{\"rounds\":[{\"player1\":\"paper\",\"player2\":\"rock\",\"winner\":1},{\"player1\":\"rock\",\"player2\":\"paper\",\"winner\":2},{\"player1\":\"paper\",\"player2\":\"scissors\",\"winner\":2}]}', NULL, 'ended'),
(3, '2019-11-25 18:08:17', 1, 2, '{\"rounds\":[{\"player1\":\"paper\",\"player2\":\"rock\",\"winner\":1},{\"player1\":\"paper\",\"player2\":\"scissors\",\"winner\":2},{\"player1\":\"paper\",\"player2\":\"rock\",\"winner\":1}]}', NULL, 'ended'),
(4, '2019-11-25 18:08:35', 1, 2, '{\"rounds\":[{\"player1\":\"rock\",\"player2\":\"paper\",\"winner\":2},{\"player1\":\"scissors\",\"player2\":\"paper\",\"winner\":1},{\"player1\":\"rock\",\"player2\":\"scissors\",\"winner\":1}]}', NULL, 'ended'),
(5, '2019-11-25 18:11:04', 1, 2, '{\"rounds\":[{\"player1\":\"paper\",\"player2\":\"dog\",\"winner\":2},{\"player1\":\"dog\",\"player2\":\"string\",\"winner\":2}]}', NULL, 'ended');

-- --------------------------------------------------------

--
-- Table structure for table `player`
--

DROP TABLE IF EXISTS `player`;
CREATE TABLE IF NOT EXISTS `player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `player`
--

INSERT INTO `player` (`id`, `name`) VALUES
(1, 'A'),
(2, 'B');

-- --------------------------------------------------------

--
-- Table structure for table `rule`
--

DROP TABLE IF EXISTS `rule`;
CREATE TABLE IF NOT EXISTS `rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `move` varchar(45) DEFAULT NULL,
  `kills` varchar(45) DEFAULT NULL,
  `editable` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rule`
--

INSERT INTO `rule` (`id`, `move`, `kills`, `editable`) VALUES
(1, 'paper', 'rock', 0),
(2, 'rock', 'scissors', 0),
(3, 'scissors', 'paper', 0),
(4, 'scissors', 'string', 1),
(5, 'string', 'dog', 1),
(6, 'dog', 'paper', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
