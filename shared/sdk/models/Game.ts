/* tslint:disable */

declare var Object: any;
export interface GameInterface {
  "id": number;
  "created_at": Date;
  "player1_id": number;
  "player2_id"?: number;
  "status"?: string;
  "game_sequence"?: string;
  "winner"?: number;
}

export class Game implements GameInterface {
  "id": number;
  "created_at": Date;
  "player1_id": number;
  "player2_id": number;
  "status": string;
  "game_sequence": string;
  "winner": number;
  constructor(data?: GameInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Game`.
   */
  public static getModelName() {
    return "Game";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Game for dynamic purposes.
  **/
  public static factory(data: GameInterface): Game{
    return new Game(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Game',
      plural: 'Games',
      path: 'Games',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
        "created_at": {
          name: 'created_at',
          type: 'Date'
        },
        "player1_id": {
          name: 'player1_id',
          type: 'number'
        },
        "player2_id": {
          name: 'player2_id',
          type: 'number'
        },
        "status": {
          name: 'status',
          type: 'string'
        },
        "game_sequence": {
          name: 'game_sequence',
          type: 'string'
        },
        "winner": {
          name: 'winner',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
